# The PiQuantum Project

**A Rasbperry Pi Quantum Computing Simulator that you can build yourself** 

<img src="res/cad.png" alt="QETLabs Logo" width="600"/>

## More Details

Please visit our documentation [here](https://johnrscott.gitlab.io/PiQuantum) for detailed information about the project, including instructions how to build it.

## Acknowledgements 

We would like to thank the QETLabs outreach team (at the University of Bristol) for funding the PiQuantum project, helping us to design and build the prototype, and field testing it at several outreach events. We think it's been a great success!

So far, QETLabs is the proud owner of the only (to our knowledge) PiQuantum device, shown in the picture below.

todo... add the picture.

The enclosure is based on an aluminium flight case, which makes it possible to take PiQuantum around and about without worrying about damaging it.

If you successfully make PiQuantum, then please send us photos, and we will make a PiQuanta picture gallery!

You can read more about QETLabs and what they do [here](https://www.bristol.ac.uk/qet-labs/).

<img src="res/qetlabs.jpg" alt="QETLabs Logo" width="100"/>

## Contact Us

John.scott@bristol.ac.uk
oliver.thomas@bristol.ac.uk
