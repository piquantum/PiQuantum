set_property PACKAGE_PIN H5 [get_ports led0]
set_property PACKAGE_PIN J5 [get_ports led1]
set_property IOSTANDARD LVCMOS33 [get_ports led0]
set_property IOSTANDARD LVCMOS33 [get_ports led1]

set_property IOSTANDARD LVCMOS33 [get_ports led2]
set_property PACKAGE_PIN T9 [get_ports led2]

set_property IOSTANDARD LVCMOS33 [get_ports led3]
set_property PACKAGE_PIN T10 [get_ports led3]

set_property PACKAGE_PIN G6 [get_ports rgb0]
set_property IOSTANDARD LVCMOS33 [get_ports rgb0]
