Setting everything up
#####################

This section assumes that you have set up the Raspberry Pi and build the PiQuantum source code (see the sections beginning with :ref:`rpi-setup`). In addition, you should have assembled the LED board (see :ref:`board-assembly` onwards).

Follow these steps to set everything up:

1. Connect the LED board to the Rasbperry Pi GPIO pins using the ribbon cable.
2. Connect the SNES controller to the Rasbperry Pi using one of the USB ports.
3. Connect the power to the Raspberry Pi, which should start it booting up.
4. Wait until the LEDs come on. If they do, then everything worked!

If anything went wrong, see the advice in the troubleshooting section (please write me...).

Operating PiQuantum
###################

.. todo:: Add some links to some good quantum information notes, explain a qubit, single qubit gates and bloch sphere.

The user can interact with PiQuantum using the controller, each user can act on a single
qubit at a time, the currently selected one flashes. 

.. image:: ../../docs/piquantum_controller_mapping.png
   :width: 800

Performing Quantum Gates
========================

+------------+--------------------------------------------------------------------------+
| Controller | Operation                                                                |
+============+==========================================================================+
| Up         | Selects the qubit above                                                  |
+------------+--------------------------------------------------------------------------+
| Down       | Selects the qubit below                                                  |
+------------+--------------------------------------------------------------------------+
| Left       | Selects the qubit to the left                                            |
+------------+--------------------------------------------------------------------------+
| Right      | Selects the qubit to the right                                           |
+------------+--------------------------------------------------------------------------+
| Y (Green)  | *C-NOT* entangling gate                                                  |
+------------+--------------------------------------------------------------------------+
| X (Blue)   | Displays the state as a superposition of computational basis states by   |
|            | cycling through each non-zero amplitude in the state                     |
+------------+--------------------------------------------------------------------------+
| A (Red)    | Hadamard gate on the selected qubit, the *superposition gate*            |
+------------+--------------------------------------------------------------------------+
| B (Yellow) | *Pauli X* gate on the selected qubit, the *NOT* gate in the computational|
|            | basis                                                                    |
+------------+--------------------------------------------------------------------------+
| T (Purple) | The *t gate* phase rotation by :math:`\pi/4`                             |
+------------+--------------------------------------------------------------------------+
| Reset      | Resets all qubits to the all zero computational basis state              |
+------------+--------------------------------------------------------------------------+

Interpreting the LED output
===========================

The state of a single qubit can be represented as a point on the `Bloch sphere <https://en.wikipedia.org/wiki/Bloch_sphere>`_. Unfortunately we only have a single RGB LED to represent the state of a qubit so the colour does not map exactly to a unique point on the Block sphere i.e. a single quantum state. Instead we have devised our own mapping of qubit to LEDs which is explained here.

.. todo:: Add the bra-ket package for mathjax somehow

Considering only amplitudes between the two levels we label the first state
:math:`\ket{0}` and the other state as :math:`\ket{1}`, each amplitude is a complex
number between 0 and 1 and the sum of the squares must sum to 1. We map the probabilities
:math:`P_0` to the red value of the LED and :math:`P_1` to the blue value of the LED.
This means that a qubit which is in the state :math:`\ket{0}` has probability :math:`P_0
= 1` and is a completely red LED. Conversely a qubit in the state :math:`\ket{1}` would
be completely blue.
