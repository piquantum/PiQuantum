# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: /home/jrs/Documents/git/piquantum/hardware/arty-a7/sim_vitis/memtest_system/_ide/scripts/systemdebugger_memtest_system_standalone.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source /home/jrs/Documents/git/piquantum/hardware/arty-a7/sim_vitis/memtest_system/_ide/scripts/systemdebugger_memtest_system_standalone.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -filter {jtag_cable_name =~ "Digilent Arty A7-35T 210319B0C665A" && level==0 && jtag_device_ctx=="jsn-Arty A7-35T-210319B0C665A-0362d093-0"}
fpga -file /home/jrs/Documents/git/piquantum/hardware/arty-a7/sim_vitis/memtest/_ide/bitstream/sim2_wrapper.bit
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
loadhw -hw /home/jrs/Documents/git/piquantum/hardware/arty-a7/sim_vitis/sim2_wrapper/export/sim2_wrapper/hw/sim2_wrapper.xsa -regs
configparams mdm-detect-bscan-mask 2
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
rst -system
after 3000
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
dow /home/jrs/Documents/git/piquantum/hardware/arty-a7/sim_vitis/memtest/Debug/memtest.elf
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
con
