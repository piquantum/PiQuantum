What is a Quantum Computer?
###########################

A quantum computer is a device based on qubits, which are like generalised bits of the kind you would find in a normal computer. Whereas a bit can only be zero or one, a qubit can also take on lots of other values (for example, mixtures of zero and one are also valid).

Quantum computers are interesting because they can do things that are difficult for a normal computer to do. The more qubits a quantum computer has, the more difficult it is to replicate its behaviour using a normal computer. The difficulty is to do with the amount of memory that is required.

An ordinary desktop computer with 8GiB of RAM can simulate no more than about 30 qubits. A supercomputer can simulate more qubits than that, depending on how much memory it has, but there is a cut off somewhere. Adding each new qubit doubles the amount of memory needed.

If a quantum computer is built that has more qubits than an ordinary computer can simulate, quantum computers will be able to do things that classical computers cannot. This has not been achieved to date, because of the difficultly of making qubits that are 100% reliable.


What does PiQuantum Do?
#######################

PiQuantum is a small quantum computer simulator, which you can build yourself. It runs on a Rasbperry Pi, which sets an upper limit on the number of qubits it can simulator. PiQuantum simulates 16 qubits, even though it would be possible to simulate a few more.

PiQuantum requires a circuit board with 16 LEDs, and a game controller. Each LED shows the current state of one of the qubits, and the game controller lets you manipulate the qubits (by applying so-called quantum gates).

Using PiQuantum, you can get a feel for what qubits are and how they differ from ordinary bits.

What do you need to build PiQuantum?
####################################

The following is a list of things you need to be able to make PiQuantum

- **A Raspberry Pi, model 3B+ (or more recent)**

  .. image:: ../images/rpi.jpeg
     :width: 300
     :alt: The Raspberry Pi
	
  The centerpiece of PiQuantum is a Rasbperry Pi, which does the simulation of the quantum computer. We developed PiQuantum using a Rasbperry Pi 3B+, but the project should work for anything more recent. The main requirement is that it has at least 4GiB of RAM. 

- **Super Nintendo Entertainment System (SNES) Game Controller**

  .. image:: ../images/snes_controller.jpg
     :width: 300
     :alt: The SNES game controller
  
  You control the qubits using a game controller. The arrow keys allow you to move around on the 4x4 grid of qubits, and the buttons let you perform qubit operations called gates. It is important to get the right model controller. A similar one won't necessarily work.

- **The LED display board**

  .. image:: ../images/board.png
     :width: 300
     :alt: The LED board
  
  The most difficult part of the process, which involves some construction, is the board containing qubit LEDs. You will need to have a copy of the printed circuit board manufactured, buy the electronic components, and solder the components onto the board. This process is described in detail in section (ref).

- **The Rasbperry Pi 40-way GPIO ribbon cable**

  .. image:: ../images/40way-cable.jpg
     :width: 300
     :alt: The LED board
  
  You will need a ribbon cable to connect the Rasbperry Pi to the LED board. THe colour does not matter, nor does the length, although a shorter one will probably look neater. The type is female-to-female. Make sure that both connectors point the same way, otherwise you might need to have the Rasbperry Pi upside down!
