/* 
 * Copyright (C) 2021 Oliver Thomas and John Scott.
 *
 * This file is part of PiQuantum, the Rasbperry Pi quantum computer
 * simulator.
 *
 * PiQuantum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PiQuantum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PiQuantum.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file controller header for controller object
 * \authors O Thomas, J Scott
 * \brief header for all usb gamepad controller things
 */

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <iostream>
#include <fstream>  // reading /dev/input/*
#include <string>
#include <cerrno>   // file reading errors 
#include <cstring>  // for unsigned chars...
#include <vector> 

#include <future> // std::async and std::future

/// Manages the literal gamepad controller 
/// \TODO replace std::string with scoped enums 
class Controller
{
    private:
        std::string loc;
        std::ifstream controller_file;
        // this is how many bytes to read in from /dev/input/*
        const int size = 16;

        // takes the 16bytes and returns a string for the button pressed
        std::string decode_bytes(const std::vector<int> & values);

        /// It would be nice if each controller stores the most recent input
        /// so it can be read and start the thread again without having to 
        /// implement it in main. Not sure atm how it would work though
        std::future<std::string> input;
    public:

        /// ctor takes the path to controller e.g. /dev/input/js0
        Controller(std::string path);

        /// return the next input from the controller 
        std::string get_input(void);

        /// wait specifically for a direction.
        std::string get_direction(void);

        /// return anything but a direction
        std::string get_btn(void);

};
#endif 
