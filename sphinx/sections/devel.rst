====================
 How the Code Works
====================

.. note:: Who knows.

The code is split into two main sections, the hardware specific code which deals with,

* Mapping the controller input
* Interfacing with LEDs (featuring software driven PWM for brightness control and custom
  flashing)
* Interfacing with the PCB buttons 
* Managing the SpiChannel 
* Wiringpy - something not too sure
* Some timer if we still use that... 

The second part deals with the quantum state implementation, all of the state vector
manipulation, state display mapping to LEDs and quantum gates.


Hardware code
=============

Controllers
-----------

.. doxygenclass:: Controller
   :members:

LEDS
----

.. doxygenclass:: Led
   :members:

Buttons
-------

.. doxygenclass:: Button
   :members:

IO
--

.. doxygenclass:: Alarm
   :members:

.. doxygenclass:: InputOutput
   :members:

RasPI PIN mappings
------------------

.. doxygenclass:: PIN
   :members:

.. doxygenclass:: WiringPi
   :members:

SPI channels
------------

.. doxygenclass:: SpiChannel
   :members:

Maths code
==========

All of the quantum specific code is located in the *state.xpp* files.

Operators
---------

.. doxygenclass:: Operator
   :members:

.. doxygenclass:: Rotation_X
   :members:

.. doxygenclass:: Rotation_Y
   :members:

.. doxygenclass:: Rotation_Z
   :members:

.. doxygenclass:: Hadamard
   :members:


Qubits
------

There is the qubit class 

.. doxygenclass:: Qubit
   :members:

State vector
------------

.. doxygenclass:: State_vector
   :members:

==================
 General Planning
==================

This section contains plans for a new version of the PiQuantum code, that will be able to support multiple output devices (hardware board, terminal graphics, and full graphics). In addition, it should support multiple input devices (such as the keyboard, or generic game controllers).

The code should be modular so that it is easy to develop and maintain each part separately. The different parts are

* **Quantum system**

  This part should keep track of the state vector, perform requested operations such as gates or measurements, and provide information about the state t. Its interface should consist of these elements:

  * A method for the input part of the system to request operations on the state vector, such as gates or measurement.
  * A method for the output part of the system to request information about the state, such as the amplitude of a given qubit.
  * A method for initialisation or management routines to set or reset the state vector.

  *Notes/questions*

  * Maybe this part can be implemented in a single class, because it has simple well defined functionality? No need to inheritance or reconfigurability.
  
* **Input system**

  This part should read user inputs and use them to control the quantum simulator and the output system. There can be multiple input systems (keyboard, controllers, etc.) but they should all behave in the same way for the purpose of the program. Since the user input should probably be continuously polling for inputs, there should probably be a user input thread. This thread should perform the following actions, in order:

  #. Read the input device (keyboard, controller, etc.).
  #. Check which input was received.
  #. Request the corresponding operation from another system (quantum simulator or output system).
  #. Go back to the start.

  *Notes/questions*

  * There must be some correct way to communicate between threads? For example, FreeRTOS implements a queue structure where one thread can push to the queue and another thread can read from it. Maybe the C++ standard library has an equivalent. 
  * I think its best to keep threading to a minimum, but I don't think there's a way to avoid a user input thread?
  * It might be necessary to have multiple user input threads, one for each user? However, they should probably all work in the same way.

* **Output system**

  This part is responsible for displaying the internal state of the quantum system, and anything else of interest, on the screen or on the hardware board. It could either work by polling the quantum system and setting the output accordingly (in its own thread), or by waiting for its routines to be called by the quantum system or the input system (which would be synchronous, so no need for threading). Depending on the type of output, there might be a need for threading anyway.

  The system should be able to do the following:

  * Take input from either the input system or the the quantum system, or read those systems continuously by polling them.
  * Write to the output device (terminal, graphics, hardware board, etc.). Depending on the details, this might require threads or other timing, but these details should be wrapped up in the implementation of the system
  
  *Notes/questions*

  * Nothing here yet.

===================
 Specific Planning
===================

This section contains suggestions for specific ways to implement the features described in the general planning section.

Suggestion 1
============

In this proposal, user input takes place in separate threads, but all the other processes run in a synchronous loop that takes place in the main thread. This is closest to the current (legacy) implementation of PiQuantum.

.. image:: ../images/suggestion1.png
   :width: 700
   :alt: Diagram of suggestion 1


User input threads
------------------

Each user input thread is contained within a single instance derived from the UserInput class. This class is abstract, and inherited classes will implement the virtual functions to deal with specific input devices. For example, a derived class might be the KeyboardInput class. The class would need to take in its constructor some kind of shared queue object, which is how it would communicate with the other parts of the program. It would only ever write commands to the queue, never read.

Arbitrarily many UserInput-derived classes could be made, and all of them would be able to add items to the same shared queue, which means that the thread safety of that queue is important. There might be some C++ constructions that could help with this, or else it might descend into low level dealings with mutexes and condition_variables, etc.

Main thread
-----------

The rest of the program (quantum system and output system) takes place in the single main thread. A single QuantumSystem class deals with the state vector and all the operations on it. The constructor for this class needs to take the shared queue object that the UserInput writes to.

In addition, there is an OututDevice class (specialised to TerminalDevice or BoardDevice), whose job is to set the correct output corresponding to the the current state of the device. The constructor of this class needs to take the shared queue object, in case the UserInput class needs to send commands directly to the output device.

The rest of the interaction between the QuantumSystem and the OutputDevice is to do with displaying the current state of the quantum system. There are three options for how this could work, given that they are all in the same thread:

* The QuantumSystem takes a reference to an instance of the OutputDevice in its constructor, and then makes free use of the public functions from the OutputDevice for updating the display. This should work if the OutputDevice has generic virtual public functions that hide whether the actual device is the terminal, hardware board, graphics, etc. The QuantumSystem would then be in control, and would request updates to the output every time something changes (for example, if UserInput causes an operation that requires a change in the output).

* The OutputDevice takes a reference to an instance of the QuantumSystem, which is the reverse of the above. This time, since the output system would not know when the output needs to be changed, it would need to poll the QuantumSystem for changes.

* Neither of the classes could take a reference to the other. In this case, functions from both classes could be repeatedly called in an infinite while loop, and the while loop itself would be responsible for obtaining information from the QuantumSystem and passing it to the OutputDevice.

It is likely that the OutputDevice might need to use threading internally in order to keep track of the shared queue. Also, specific instances might need threading or timing to implement their functions (e.g. the BoardDevice needs to emulate PWM in software).

Suggestion 2
============

See the folder ``plan/suggestion2``.



Notes
=====

Reconfigurable backends 

#. PiQuantum pcb
#. Terminal (ncurses)
#. OpenGL

Reconfigurable front-end

#. (Gamepad) Controllers 
#. Keyboard
#. TODO Add support for all controllers 


Optional
========

# Sound effects 



=========
 License
=========

The code and documentation is covered by the GNU General Public License below.

.. literalinclude:: ../../COPYING
