`timescale 1ns / 1ps

module memtest_tb();

    reg clk, clk_ref, start, rst;

    

    memory memory_0 (
        .sys_clk_i(clk),
        .clk_ref_i(clk_ref),
        .sys_rst(rst),
        .start(start)
    );
    
    ddr3_model ddr3_0 (
        
    
    
    
    );
    
    // System clock
    initial begin
        clk = 0;
        forever begin
            #2 clk = ~clk; 
        end
    end

    // Reference clock
    initial begin
        clk_ref = 0;
        forever begin
            #5 clk_ref = ~clk_ref; 
        end
    end
    
    // Test
    initial begin
        start = 0;
        rst = 1;
        
        // Reset
        #50 rst = 0;
        #50 rst = 1;
        #1000;
        
        // Test
        start = 1;
        #40 start = 0;
    
    end

endmodule
