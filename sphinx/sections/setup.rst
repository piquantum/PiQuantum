Getting and setting up a Raspberry Pi
#####################################

The first step is to enable the SPI peripheral on the Rasbperry Pi. Open the Rasbperry Pi configuration options by running:

.. code-block:: console

   $ sudo raspi-config

Select ``SPI`` inside ``Interfacing Options``. Press enter, and confirm that you want to enable SPI. Then reboot the Rasbperry Pi.
   
The next step is to install a dependency, Eigen3, as follows:

.. code-block:: console

   $ sudo apt install libeigen3-dev

Eigen3 is a C++ library for performing linear algebra computations. PiQuantum's quantum computer simulation is based on Eigen3.

If you want the optional software rendering, you also need the ncurses library which is
also avaliable on the software repository, 

.. code-block:: console
   
   sudo apt-get install libncurses-dev

Getting the Simulator Source Code
#################################

You can get the source code for PiQuantum by cloning the GitLab repository `here <https://gitlab.com/johnrscott/piquantum>`_. To do this, open a terminal on the Raspberry Pi, navigate to any folder, and run

.. code-block:: console

   $ git clone https://gitlab.com/johnrscott/piquantum.git

This will copy the source code into a folder called ``piquantum/``.

Building the Code
#################

Navigate into the folder ``piquantum/`` folder. From there, run

.. code-block:: console

   $ make

This should produce a file called ``piquantum.bin``. If you get any errors, have a look at the troubleshooting section (ref). 

Running the Code
################

To run the code, navigate into the ``piquantum/`` folder and execute the following line:

.. code-block:: console

   $ sudo ./piquantum.bin

The program needs root privileges because it uses the GPIO ports.

.. todo:: The is probably a way to remove the root requirement, add the user to some group or other?

Installing the program
######################

.. todo:: Think of a good way to have the program run on start up that is easily reversible and doesn't mess up the Raspberry PI root system!
