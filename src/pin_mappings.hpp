/* 
 * Copyright (C) 2021 Oliver Thomas and John Scott.
 *
 * This file is part of PiQuantum, the Rasbperry Pi quantum computer
 * simulator.
 *
 * PiQuantum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PiQuantum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PiQuantum.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file pin_mappings.hpp 
 * \brief Chip/line structure 
 *
 * Store the chip and line number for 
 * a particular LED or button.
 *
 */

#include <vector>

typedef struct 
{
    int chip;
    int line;
} Position;

/// use gpio readall to get wiringPi mappings
class PIN
{
    public:
        
        /// for leds
        static const int LE = 0; ///< Physical pin 11
        static const int OE = 1; ///< Physical pin 12
       
        /// for buttons
        static const int SHLD = 23; ///< Physical pin 33
};

/*
std::vector<std::vector<Position> > led_pos{ 
            { {6,0}, {6,2}, {6,1} },
            { {6,3}, {6,5}, {6,4} },
            { {0,3}, {0,5}, {0,4} },
            { {0,0}, {0,2}, {0,1} },

            { {5,0}, {5,2}, {5,1} },
            { {6,6}, {5,3}, {6,7} },
            { {0,6}, {1,3}, {0,7} },
            { {1,0}, {1,2}, {1,1} },

            { {5,7}, {5,5}, {5,6} },
            { {4,1}, {5,4}, {4,0} },
            { {2,1}, {1,4}, {2,0} },
            { {1,7}, {1,5}, {1,6} },

            { {4,7}, {5,5}, {5,6} },
            { {4,4}, {4,2}, {4,3} },
            { {2,4}, {2,2}, {2,3} },
            { {2,7}, {2,5}, {2,6} } };

*/
