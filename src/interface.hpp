/* 
 * Copyright (C) 2021 Oliver Thomas and John Scott.
 *
 * This file is part of PiQuantum, the Rasbperry Pi quantum computer
 * simulator.
 *
 * PiQuantum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PiQuantum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PiQuantum.  If not, see <https://www.gnu.org/licenses/>.
 */

/** 
 * \file interface.hpp
 * \authors J Scott, O Thomas
 * \date Feb 2019 
 *
 * \detail LED and Button control. 
 *
 */

#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include "io.hpp"

/**
 * \brief RGB Led class
 *
 * \detail Objects of this class represent RGB LEDs. When the class is 
 * instantiated, it creates an InputOutput class driver automatically (if
 * on does not already exist) and then registers itself with it, so that
 * its RGB values are automatically sent to the device.
 *
 * The constructor takes three arguments which are the positions on the 
 * driver chips of the R, G and B lines. They are specified in the form
 * {chip, line}, where chip is the chip number and line is the line number
 * on that chip.
 *
 * RGB values are updated using the set_rgb function. RGB values are between
 * 0 and 1, with 0 representing off and 1 representing maximum brightness. The
 * new state will be written immediately to the LEDs. To turn the LED off write
 * RGB values of 0.
 *
 */
class Led {
    private:

        std::shared_ptr<InputOutput> driver; // Underlying hardware driver  
        int id; // Used to identify the LED to the driver
        std::vector<Position> positions; // Chip and line numbers 
        std::vector<double> rgb; // RGB values (0 to 1): In order [0]=r, [1]=g, [2]=b

    public:

        /// ctor takes the *Position* of the red, green and blue pins of a physical LED
        Led(Position r, Position g, Position b); 

        /// ctor takes a vector of the red, green, blue pins of a physical LED
        Led(std::vector<Position> pos); // takes vector for RGB vals

        //~Led(); // Destructor
        
        /// writes RGB values 
        void set_rgb(double red, double green, double blue); 
        /// read the current RGB values and returns std::vector
        std::vector<double> get_rgb(void);  
        /// Return the chip and line numbers
        std::vector<Position> get_positions(void); 
};

class Button {
    private:
        // A pointer to the LED driver which writes
        // data to the hardware. Each Led object
        // needs to register itself with the
        // driver. The driver then polls the
        // Led objects to read their color and
        // write it to the hardware device
        std::shared_ptr<InputOutput> driver;
        int id; /// Identifying the Button object to the driver

        /// Chip and line numbers 
        const Position position;

        /// RGB values (between zero and one)
        std::vector<double> rgb_values; // In order [0]=r, [1]=g, [2]=b

        /// Button state
        int btn_state;

        /// So that InputOutput can set the btn_state. I'd prefer it to
        /// only be able to access the btn_state variable but haven't
        /// figured out a good way to do it yet
        friend class InputOutput;

    public:

        // Constructor and destructor
        Button(Position position);
        //~Button();

        /// Read the button state
        int get_state(void);

        /// Return the chip and line numbers
        Position get_position(void);
};

#endif
